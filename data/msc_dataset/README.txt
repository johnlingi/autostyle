Dataset used for MSc project.

Contains Twitter tweets labeled with the emotions joy or anger. The longest sequence length is 10 tokens (not including
the <eos> special token). The data statistics are as follows:

Training:
anger: 1718
joy: 2376

Validation:
anger: 191
joy: 265

Testing:
anger: 213
joy: 294