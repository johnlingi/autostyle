"""
Script to extract IDs from the Wang dataset which have a certain emotion.

These will be used for experimentation.

Here we take the emotions joy, anger and fear. The size of the new datasets are:

train: 141741
dev: 140870
test: 142605

"""

import pandas as pd
import os

def extract_ids(file, save_file, emotions):
    """
    Takes the lines from the file which have the desired emotion.

    Assumes that data is formatted in two columns separated by tabs, with the first column as the ID and the second
    as the emotion.

    :param file: string, file to load data from
    :param save_file: string, file to save filtered ids to
    :param emotions: array of strings, used to extract ids
    :return: None
    """

    data_df = pd.read_csv(file, sep='\t', header=None, names=["id", "emotion"])
    data_df = data_df[data_df["emotion"].isin(emotions)]
    # data_df.to_csv(save_file, sep="\t", header=False, index=False)

    print("Number of filtered data points", data_df.shape[0])

if __name__ == "__main__":
    current_dir = os.getcwd()

    data_file = current_dir + "/train.txt"
    save_file = current_dir + "/train_filtered.txt"
    extract_ids(data_file, save_file, emotions=["joy", "anger", "fear"])




