Autostyle contains a model for changing the emotional expression in Twitter messages. This is achieved using a variant of
an encoder-decoder model. Because no parallel data is available for the different emotions, the model has been trained
using an emotion classifier to control the emotion in the outputted sentence, and an auto-encoder to control the
linguistic structure of the sentence. The model is currently built only for research purposes.

Requirements:
Keras 2.2 (TensorFlow backend)
Python 3
GloVe Embeddings (https://nlp.stanford.edu/projects/glove/ - download glove.twitter.27B.zip)

How to run:
To train and run the model, load the jupyter notebook titled "autostyle_main" found in the model/style_transfer
directory. Because of the nature of the model, we are currently having difficulties saving the model after training. This
needs to be sorted immediately either by finding ways around this.

For the GloVe embeddings, put these in a folder called 'glove' inside the data folder.

TODO
In order of priority:
- Either find a way to save the model or port try porting to a different Deep Learning framework. The current error
is that some layers cannot be serialised. I believe this is due to the complex connections in the model between the decoder
and the attention mechanism.
- Improve the model's performance either with a new architecture or finding ways to tune the current model
- Implement a bespoke Attention layer in Keras.
- Build the data pipeline to take a raw tweet and pass through inference (To do this, I'll have to reupload the Ekphrasis
package).