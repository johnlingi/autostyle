"""
This script gathers tweets from the Twitter API.

It is used to collect tweets provided by Wang et al.

"""

import tweepy
import os

consumer_key = "dE8TVo0SNYJxuR6KEcKeKpGI2"
consumer_secret = "u0jAsMEcQkPguj6omOqnRfpIDbfq7R1KZNQNzhOLE4nWwgAMFt"
access_token = "2799236528-OoWkw4Ay1SzZUOxLeJuXXBpFYgPQWgkgqIpDwGP"
access_token_secret = "TAeFO97nu2Glx4qzSQuldDPDR06O2Za9cifzaOrey6dAc"


def append_to_file(file, data):
    """
    Write the tweets to a text file

    :param file: text file
    :param data: [tweet_id, tweet, emotion], where each element is 100 long
    :return:
    """
    with open(file, 'a') as f:
        for i in range(len(data[0])):
            line = "{}\t{}\t{}\n".format(data[0][i], data[1][i], data[2][i])
            f.write(line)


def scrape(id_file, save_file):
    """
    Scrapes twitter tweets in batches of 100.

    :param id_file: file containing tweet ids
    :param save_file: file to write data to (appended)
    :return:
    """

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
    requests = 100  # Batch size, max for tweepy package

    with open(id_file, 'r') as f:
        tweet_ids = [0] * requests
        emotions = [0] * requests
        for number, line in enumerate(f):
            tweet_ids[number % requests], emotions[number % requests] = line.strip().split(sep='\t')
            if number % requests == 0 and number != 0:
                statuses = api.statuses_lookup(tweet_ids)
                tweets = [status.text for status in statuses]
                append_to_file(save_file, [tweet_ids, tweets, emotions])

                return


if __name__ == "__main__":

    current_dir = os.getcwd()
    source_dir = os.path.dirname(current_dir)

    id_file = source_dir + "/data/wang_dataset/train_ids.txt"
    save_file = source_dir + "/data/wang_dataset/train_full.txt"

    scrape(id_file, save_file)