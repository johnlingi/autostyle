"""
Utility functions for scripts in model package

Author: John Lingi
Date: 07/18
"""
import numpy as np
from keras.utils import to_categorical

    
def load_classifier(source_dir, vocab_size):
    return source_dir + "/model/binary_classifier/pre_trained/deepmoji-{}-weights.h5".format(vocab_size)


def text_to_sequences(data, word2id, max_sequence_length, pad_index=0, unknown_token="<unk>"):
    """
    This converts an array of sentences into a fixed length sequence of integers

    :param data: array of strings
    :param word2id: dictionary containing (string, integer) pairs for tokens and their id
    :param pad_index: id to use for padding
    :return 2d integers
    """
    unknown_id = word2id[unknown_token]
    sequences = []
    for tweet in data:
        t_zero = [pad_index] * max_sequence_length

        # Create integer sequence for training data
        for index, word in enumerate(tweet.split()):
            t_zero[index] = word2id[word] if word2id.get(word) is not None else unknown_id

        sequences.append(t_zero)

    return sequences


def sequence_to_text(sequence, id2word):
    """
    Converts sequences of integers to text

    :param sequence: 2d array of integers (numpy or python)
    :param id2word: dictionary containing (integer, string) pairs for tokens and thier id
    :return: array of strings
    """

    if type(sequence) == np.ndarray:
        text_sequences = [0] * sequence.shape[0]
    else:
        text_sequences = [0] * len(sequence)

    for i in range(len(text_sequences)):
        text_array = []
        for token_id in sequence[i]:
            text_array.append(id2word[token_id])

        text_sequences[i] = " ".join(text_array)

    return text_sequences


def one_hot_data(data, word2id, max_sequence_length=None, pad_index=0, unknown_token="<unk>"):
    """
    One-hot encodes tweet data. 0 represents paddings and returns a vector which is [1, 0....,0]

    :param data: Array of strings
    :param word2id:
    :param pad_index: id to use for padding
    :return: numpy array of size (num sequences, max sequence length, vocabulary size)
    """

    vocab_size = len(word2id)
    if max_sequence_length is None:
        max_sequence_length = len(max(data))
 
    return to_categorical(text_to_sequences(data,
                                             word2id,
                                             max_sequence_length,
                                             pad_index,
                                             unknown_token), vocab_size)


def create_embedding_matrix(vocab_size, embedding_dim, word2id, embeddings_index):
    """
    Creates the embedding matrix

    :param max_words: maximum number of words in vocabulary
    :param embedding_dim: size of embedding
    :param word2id: dictionary containing (string, integer) pairs for tokens and their id
    :param embeddings_index: embedding dictionary with token and vector
    :return:
    """
    embedding_matrix = np.zeros((vocab_size, embedding_dim))
    found = 0
    for word, i in word2id.items():
        if i < vocab_size:
            embedding_vector = embeddings_index.get(word)

            # words not found will be all zero
            if embedding_vector is not None:
                found += 1
                embedding_matrix[i] = embedding_vector
            else:
                embedding_matrix[i] = np.random.random(size=embedding_dim)

    print("Found {0} from {1} words in vocab".format(found, len(word2id)))
    return embedding_matrix


def remove_sentences(data, labels, length):
    """
    Removes data which exceed the maximum length specified.
    
    :param data: array of strings
    :param labels: corresponding labels for the training data
    :param length: maximum length to filter
    """
    filtered_data = []
    filtered_labels = []
    for tweet, label in zip(data, labels):
        if len(tweet.split()) <= length:
            filtered_data.append(tweet)
            filtered_labels.append(label)
    
    return filtered_data, filtered_labels


def add_special_tokens(data, start=False, end=True, start_token="<go>", end_token="<eos>"):
    """
    Add tokens to the beginning and end of all sentences

    :param data: array of strings
    :param start: prepend start token
    :param end: append end token
    """
    
    new_data = [0] * len(data)
    for i in range(len(data)):
        if end and not start:
            new_data[i] = data[i] + (" " + end_token)
        elif start and not end:
            new_data[i] = start_token + " " + data[i]
        else:
            new_data[i] = start_token + " " + data[i]
            new_data[i] = data[i] + (" " + end_token)
        
    return new_data