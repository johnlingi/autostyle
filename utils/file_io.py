"""
This file contains utility functions that support saving and loading files in different formats.

Author: John Lingi
Date: 06/18
"""

import pickle
import numpy as np
from operator import itemgetter


def save_frequency_dict(file, token_count):
    """
    Function to save the token frequency dictionary. The format of the dictionary is (string, int)

    :param file:
    :param token_count:
    :return:
    """

    print("Writing dictionary to file '", file, "'.")
    with open(file, "w") as f:
        # Sorts by value from biggest to smallest. .items() returns a tuple (key, value), itemgetter(1) gets the value
        for key, value in sorted(token_count.items(), key=itemgetter(1), reverse=True):
            line = key + '\t' + str(value) + '\n'
            f.write(line)


def save_to_text(file, data):
    """
    Saves the data to a text file

    :param data: array of string sentences
    :return:
    """
    print("Writing data to", file)
    with open(file, "w") as f:
        for sequence in data:
            f.write(sequence + '\n')


def load_from_text(file):
    """
    Loads sentences from file

    :return: array of strings
    """

    data = []
    print("Reading data from", file)
    with open(file, "r") as f:
        for line in f.readlines():
            data.append(line.strip())

    return data


def save_pickle(file, data):
    """
    Saves data to a pickle file.
    """
    with open(file, "wb") as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)


def load_pickle(file):
    """
    Loads data from a pickle file

    :return: python object
    """
    with open(file, "rb") as f:
        data = pickle.load(f)

    return data


def load_glove(glove_file):
    """
    Loads word embeddings

    :param glove_file: File must be provided.
    :return:
    """
    embeddings_index = {}
    with open(glove_file) as f:
        for line in f:
            values = line.split()
            word = values[0]
            vector = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = vector
    
    print("Found {} word vectors".format(len(embeddings_index)))

    return embeddings_index