This directory contains scripts for producing a vocabulary and also saved vocabulary files stored as pickle files.

A vocabulary is built by first counting the number of times each token appears in the data. Using this information,
the vocabulary is set to be all the tokens appearing more times than a minimum threshold. In addition, the special tokens
<unk>, <go>, <eos> and <pad> are added to the vocabulary.