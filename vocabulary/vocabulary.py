"""
Functions for calculating token frequencies in the tweets and building vocabulary

Modified from Tianxiao shen et al.
source: https://github.com/shentianxiao/language-style-transfer
"""

import pickle
import os


def token_frequency(tweets):
    """
    Returns a dictionary containing a list of tokens and their frequency

    :param tweets: array of tweets
    :return: dictionary
    """
    token_count = dict()

    for tweet in tweets:
        tokens = tweet.split(sep=" ")

        for token in tokens:
            if token == " " or token == "":
                continue

            if token_count.get(token) is None:
                token_count[token] = 1
            else:
                token_count[token] += 1

    return token_count


def build_vocab(data_file, save_file, min_occur=5):
    """
    This function builds vocabulary  and saves it to a file

    :param data_file: file containing frequency of tokens
    :param save_file: file to save vocabulary
    :param min_occur: minimum frequency threshold. Any tokens appearing less than this are ignored.
    :return:
    """

    word2id = {'<pad>': 0, '<go>': 1, '<eos>': 2, '<unk>': 3}
    id2word = ['<pad>', '<go>', '<eos>', '<unk>']
    vocab_size = len(word2id)

    with open(data_file, 'r') as f:
        for line in f.readlines():
            word, frequency = line.strip().split()
            if int(frequency) >= min_occur:
                word2id[word] = len(word2id)
                id2word.append(word)
                vocab_size += 1

    print("Vocabulary size:", vocab_size)

    with open(save_file, 'wb') as f:
        pickle.dump((vocab_size, word2id, id2word), f, pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":


    # Build vocabulary
    current_dir = os.getcwd()
    source_dir = os.path.dirname(current_dir)

    min_c = 1
    save_file = "supervised_data_vocab_10_min_angry_joy" + str(min_c) + ".pl"
    vocab_file = source_dir + "/data/combined/10_tokens/supervised_data/supervised_data_10_token_frequency_count_angry_joy.txt"
    build_vocab(vocab_file, save_file, min_occur=min_c)