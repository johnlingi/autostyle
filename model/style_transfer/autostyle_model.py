"""
This model is a model that combines an autoencoder and a style classifier to learn how to convert the emotional
content of a sentence. The classifier used here is a pre-trained model called DeepMoji (https://github.com/bfelbo/DeepMoji)

This model takes in a style label input as well as the sentence to enable flexible transfer between both.
This is different from the deepmoji autoencoder model which only takes in one type of sentence. The theory is that the
model will be improved if it can see the distribution from both corpora.

We call this the autostyle model

This includes embedding and attention
"""

from operator import itemgetter
import h5py
import os
import numpy as np
from model.style_transfer.samplers import greedy_sampling, temperature_sampling
from keras.models import Model
from keras.layers import LSTM, Dense, Input, Lambda, RepeatVector, \
    Concatenate, Dot, Reshape, Embedding, GaussianNoise, Bidirectional, concatenate
from keras import optimizers, regularizers
from keras import backend as K
import matplotlib.pyplot as plt
from keras.activations import softmax

from model.style_transfer.attlayer import AttentionWeightedAverage


def softmax_axis1(x):
    """
    Softmax along axis 1 rather than axis 0
    :param x: input vector
    :return: softmax of input
    """
    return softmax(x, axis=1)


class AutoStyle(object):

    def __init__(self,
                 word2id,
                 max_timestep,
                 embedding_matrix,  # (N, M) matrix
                 num_styles=2,
                 latent_dims=20,
                 attention_dims=20,
                 start_character="<go>",
                 stop_character="<eos>",
                 dropout=0,
                 attention=True,
                 decoder_embedding_matrix=None,
                 bilstm=False
                 ):

        self.latent_dims = latent_dims
        self.num_styles = num_styles
        self.attention_dims = attention_dims
        self.input_dims = len(word2id)  # One-hot encoding
        self.word2id = word2id
        self.start_character = start_character
        self.stop_character = stop_character
        self.max_timestep = max_timestep
        self.attention = attention
        self.bilstm = bilstm

        # Training Params
        self.history = None
        self.epochs = 0
        self.batch_size = 0
        self.optimiser = ""
        self.learning_rate = 0
        self.dropout = dropout

        self.model = None

        # Decoder has the same dimensions as encoder
        if self.bilstm:
            self.decoder_latent_dims = latent_dims * 2
        else:
            self.decoder_latent_dims = latent_dims

        # Define layers
        # Model inputs
        self.encoder_inputs = Input(shape=(self.max_timestep,), name="encoder_inputs")  # Sequences of integers
        self.encoder_style_label = Input(shape=(self.num_styles,), name="encoder_style_label")  # N x num style labels
        self.decoder_style_label = Input(shape=(self.num_styles,),
                                         name="decoder_style_label")  # N x num style labels (1 because same for each timestep)

        # Embedding
        self.embedding = Embedding(input_dim=self.input_dims, output_dim=embedding_matrix.shape[1],
                                   input_length=self.max_timestep, name="embedding",
                                   trainable=False, weights=[embedding_matrix])

        if decoder_embedding_matrix is not None:
            self.decoder_embedding = True
            self.embedding_decoder = Embedding(input_dim=self.input_dims, output_dim=decoder_embedding_matrix.shape[1],
                                           name="embedding_decoder", input_length=1,
                                           trainable=False, weights=[decoder_embedding_matrix]) # process one input at a time
        else:
            self.decoder_embedding = False

        # Encoder
        if self.bilstm:
            self.encoder = Bidirectional(LSTM(self.latent_dims, return_sequences=True, return_state=True, dropout=self.dropout),
                            name="encoder")  # Layer twice as big now

        else:
            self.encoder = LSTM(self.latent_dims, return_sequences=True, return_state=True, dropout=self.dropout, name="encoder")  # Layer twice as big now

        # Attention
        self.att_dense = Dense(self.attention_dims, activation="tanh", name="att_dense")
        self.att_softmax = Dense(1, activation=softmax_axis1, name="att_softmax")
        self.context_vector = Input(shape=(1, self.latent_dims), name="context_vector")  # shape[0] is 1 because feed one step at a time

        # Decoder
        self.decoder_inputs = Input(shape=(1, ), name="decoder_inputs_forward")
        self.decoder = LSTM(self.decoder_latent_dims, return_sequences=True, return_state=True, dropout=self.dropout, name="decoder")
        self.decoder_softmax_output = Dense(self.input_dims, activation="softmax", name="decoder_softmax_output")

        # DeepMoji Classifier
        self.dm_dense = Dense(256, activation='tanh', kernel_regularizer=regularizers.l2(1e-6), name="dm_dense")
        self.dm_lstm_0_output = Bidirectional(LSTM(512, return_sequences=True), name="dm_bi_lstm_0")
        self.dm_lstm_1_output = Bidirectional(LSTM(512, return_sequences=True), name="dm_bi_lstm_1")
        self.dm_attention = AttentionWeightedAverage(name='dm_attlayer')
        self.dm_softmax = Dense(1, activation='sigmoid', name='dm_softmax')

        # Auxilliary
        self.max_ts_repeat = RepeatVector(self.max_timestep, name="max_timestep_repeat")
        self.one_ts_repeat = RepeatVector(1, name="decoder_style")
        self.conc_timestep = Concatenate(axis=1, name="concatenate_timestep")
        self.conc_dec = Concatenate(axis=-1, name="concatenate_decoder")

    def build(self, noise=0.01, deepmoji_weights=None, all_model_weights_file=None,):
        """
        Builds the model

        :param noise: Gaussian noise
        :param deepmoji_weights: Pre-trained weights for deepmoji with a sigmoid output. We will extract the
        bilstm, attention and softmax weights.
        :param all_model_weights_file: weights for pre-trained DMAutoStyle model
        :return: model
        """

        # Encoder
        print("Initialising Encoder Forward...", end="", flush=True)
        embedding_output = self.embedding(self.encoder_inputs)

        # Noise layer
        embedding_output = GaussianNoise(noise)(embedding_output)

        # Encoder Style input
        style_labels = self.max_ts_repeat(self.encoder_style_label)  # Use RepeatVector because style label is N x num_styles
        concat_input = Concatenate(axis=-1)([embedding_output, style_labels])  # Shape = (N, MAX TS, Embed dims + num_styles)

        if self.bilstm:
            all_encoder_states, forward_h, forward_c, backward_h, backward_c = self.encoder(concat_input)  # encoder_output is an array
            state_h = Concatenate()([forward_h, backward_h])
            state_c = Concatenate()([forward_c, backward_c])

        else:
            all_encoder_states, state_h, state_c = self.encoder(concat_input)
        # Collect the final output and cell state to pass to the decoder
        print("done")

        # Decoder
        print("Initialising Decoder Forward...", end="", flush=True)
        # Forward Transfer
        softmax_output = self._decoder_serial_input(all_encoder_states, state_h, state_c)
        print("done")

        # Classifier layer
        print("Initialising Classifier Forward...", end="", flush=True)
        x = self.dm_dense(softmax_output)
        lstm_0_output = self.dm_lstm_0_output(x)
        lstm_1_output = self.dm_lstm_1_output(lstm_0_output)

        x = concatenate([lstm_1_output, lstm_0_output, x])
        x = self.dm_attention(x)
        classifier_output_forward = self.dm_softmax(x)

        print("done")

        self.model = Model(inputs=[self.encoder_inputs, self.encoder_style_label, self.decoder_inputs, self.decoder_style_label],
                           outputs=[classifier_output_forward, softmax_output])

        if all_model_weights_file is not None:
            self.model.load_weights(all_model_weights_file)

        elif deepmoji_weights is not None:
            self.load_dm_weights(deepmoji_weights, exclude_names=[])  # dense_9 because didn't name it when trained

        print("\n\n")
        print("Note: Before compiling, some parameters may still appear trainable even when set to untrained.")
        print(self.model.summary())

    def build_classifier(self, deepmoji_weights=None):
        """
        Builds the deepmoji classifier independently. This can be used for evaluation.
        """
        
        inp = Input(shape=(self.max_timestep, self.input_dims))
        x = self.dm_dense(inp)
        lstm_0_output = self.dm_lstm_0_output(x)
        lstm_1_output = self.dm_lstm_1_output(lstm_0_output)

        x = concatenate([lstm_1_output, lstm_0_output, x])
        x = self.dm_attention(x)
        classifier_output_forward = self.dm_softmax(x)
        
        self.classifier = Model(inputs=[inp], outputs=[classifier_output_forward])
        
        if deepmoji_weights is not None:
            self.load_dm_weights(deepmoji_weights, exclude_names=[])

    def get_weights_from_h5(self, weights_file):
        """
        Loads the weights from a saved Keras model into numpy arrays.
        The weights are saved using Keras 2.0 so we don't need all the
        conversion functionality for handling old weights.

        source: https://github.com/bfelbo/DeepMoji
        """

        with h5py.File(weights_file, mode='r') as f:
            layer_names = [n.decode('utf8') for n in f.attrs['layer_names']]
            layer_weights = []
            for k, l_name in enumerate(layer_names):
                g = f[l_name]
                weight_names = [n.decode('utf8') for n in g.attrs['weight_names']]
                weight_values = [g[weight_name][:] for weight_name in weight_names]
                if len(weight_values):
                    layer_weights.append([l_name, weight_names, weight_values])
            return layer_weights

    def load_dm_weights(self, weights_file, exclude_names=[], verbose=True):
        """
        Loads model weights from the given file path, excluding any
                given layers.

        # Arguments:
            model: Model whose weights should be loaded.
            weight_path: Path to file containing model weights.
            exclude_names: List of layer names whose weights should not be loaded.
            extend_embedding: Number of new words being added to vocabulary.
            verbose: Verbosity flag.

        # Raises:
            ValueError if the file at weight_path does not exist.

        """
        if not os.path.exists(weights_file):
            raise ValueError('ERROR (load_weights): The weights file at {} does '
                             'not exist. Refer to the README for instructions.'
                             .format(weights_file))

        # Copy only weights from the temporary model that are wanted
        # for the specific task (e.g. the Softmax is often ignored)
        layer_weights = self.get_weights_from_h5(weights_file)

        for i, w in enumerate(layer_weights):
            l_name = 'dm_' + w[0]
            # weight_names = w[1]
            weight_values = w[2]

            if l_name in exclude_names:
                if verbose:
                    print('Ignoring weights for {}'.format(l_name))
                continue

            try:
                model_l = self.model.get_layer(name=l_name)
            except ValueError:
                raise ValueError("Weights had layer {},".format(l_name) +
                                 " but could not find this layer in model.")

            if verbose:
                print('Loading weights for {}'.format(l_name))

            model_l.set_weights(weight_values)

            model_l.trainable = False

    def _one_step_attention(self, encoder_states, decoder_state):
        """
        Perform one step of the attention during each decoding time-step. Luong implementation

        :param encoder_states: dims (N, Tx, latent_dims_encoder)
        :param decoder_state:  (N, latent_dims_decoder)
        :return: Context vector: (N, 1, lde), attention weights
        """

        s_rep = RepeatVector(self.max_timestep)(decoder_state)  # Shape = (N, Tx, ldd)
        concat = Concatenate(axis=-1)([s_rep, encoder_states])  # Shape = (N, Tx, lde + ldd)

        e = self.att_dense(concat)  # Shape = (N, Tx, attention dims)
        a = self.att_softmax(e)  # Shape = (N, Tx, 1) - softmax applied on axis 1

        context = Dot(axes=1)([a, encoder_states])  # Shape = (N, 1, lde)

        return context, a

    def _decoder_serial_input(self, encoder_states, state_h, state_c):
        """
        Compute one-by-one input to decoder, taking output from previous time-step as input
        :param encoder_states: All the encoder states
        :param state_h: starting hidden state
        :param state_c: starting cell state
        :return: Concatenated output which is shape = (N, Timestep, Input dims)
        """

        all_softmax = []
        states = [state_h, state_c]  # Could randomise these!
        inputs = self.decoder_inputs
        argmax = Lambda(K.argmax, name="argmax")

        for t in range(self.max_timestep):

            if self.decoder_embedding:
                decoder_inputs = self.embedding_decoder(inputs)  # Output shape = (N, 1, Embedding dim)
            else:
                decoder_inputs = inputs

            style_labels = self.one_ts_repeat(self.decoder_style_label)  # This is really for reshaping to (N, 1, num_styles)
            concat = self.conc_dec([decoder_inputs, style_labels])  # Join to style label and feed into decoder

            decoder_output_forward, state_h, state_c = self.decoder(concat, initial_state=states)

            if self.attention:
                context, _ = self._one_step_attention(encoder_states, state_h)  # Size of latent dims
                decoder_output_forward = Concatenate(axis=-1)([context, decoder_output_forward])

            outputs = self.decoder_softmax_output(decoder_output_forward)  # Shape = (N, 1, input dims)

            all_softmax.append(outputs)
            states = [state_h, state_c]

            if self.decoder_embedding:
                inputs = argmax(outputs)
            else:
                inputs = outputs

        # Returns all the softmaxes and the decoder states
        # These are concatenated along the timestep
        return self.conc_timestep(all_softmax)

    def train(self, x, y,
              validation=None,
              losses=["binary_crossentropy", "categorical_crossentropy"],
              batch_size=20,
              epochs=10,
              loss_weights=[1, 1],
              optimizer=optimizers.Adadelta(),
              callbacks=None):
        """
        :return: log of training
        """

        if self.model is None:
            raise Exception("You must first call the 'build' function before training.\n")

        # Params
        self.epochs = epochs
        self.batch_size = batch_size
        self.optimiser = optimizer

        self.model.compile(optimizer=optimizer, loss=losses, loss_weights=loss_weights, metrics=["acc"])

        self.history = self.model.fit(x=x, y=y, validation_data=validation,
                       batch_size=batch_size, epochs=epochs, callbacks=callbacks)

        return self.history

    def build_inference(self):
        """
        New model for inference which uses the same setup as the training model but different outputs

        :return: inference model
        """

        # Build Encoder Inference Model
        embedding_output = self.embedding(self.encoder_inputs)

        encoder_style_labels = self.max_ts_repeat(self.encoder_style_label)  # Encoder style label is (1, num_styles)
        encoder_concat = Concatenate(axis=-1)([embedding_output, encoder_style_labels])

        # No noise at inference time
        if self.bilstm:
            all_encoder_states, forward_h, forward_c, backward_h, backward_c = self.encoder(encoder_concat)  # encoder_output is an array
            state_h = Concatenate()([forward_h, backward_h])
            state_c = Concatenate()([forward_c, backward_c])
        else:
            all_encoder_states, state_h, state_c = self.encoder(encoder_concat)  # encoder_output is an array

        self.inference_encoder = Model(inputs=[self.encoder_inputs, self.encoder_style_label],
                                       outputs=[all_encoder_states, state_h, state_c])

        # Set layers to untrainable
        for layer in self.inference_encoder.layers:
            layer.trainable = False

        print("Encoder inference model summary")
        print(self.inference_encoder.summary())


        # Decoder with output at time t feeding back into input at t+1
        attention_encoder_states = Input(shape=(self.max_timestep, self.decoder_latent_dims),  # Attention has encoder states size latent dims * 2 if bidectional
                                         name="encoder_states")  # All encoder states (N, Tx, lde)
        decoder_state_input_h = Input(shape=(self.decoder_latent_dims,), name="state_h")  # Pass the states in one at a time
        decoder_state_input_c = Input(shape=(self.decoder_latent_dims,), name="state_c")  # Pass the states in one at a time
        decoder_state_inputs = [decoder_state_input_h, decoder_state_input_c]

        # Decoder Embedding
        if self.decoder_embedding:
            decoder_inputs = self.embedding_decoder(self.decoder_inputs)  # Shape = (N, 1, embedding_size)
        else:
            decoder_inputs = self.decoder_inputs

        # Decoder Style Input
        decoder_style_labels = self.one_ts_repeat(self.decoder_style_label)  # Shape = (N, 1, num_styles)
        decoder_concat = self.conc_dec([decoder_inputs, decoder_style_labels])  # Shape = (N, 1, embedding_size + num_styles)

        decoder_output_forward, state_h, state_c = self.decoder(decoder_concat, initial_state=decoder_state_inputs)
        decoder_states = [state_h, state_c]

        # Attention
        if self.attention:
            context, attention_weights = self._one_step_attention(attention_encoder_states, state_h)  # Size of latent dims

            decoder_output_forward = Concatenate(axis=-1)([context, decoder_output_forward])
            attention_weights = Reshape((int(attention_weights.shape[2]), int(attention_weights.shape[1])))(
                attention_weights)  # Reshape so row vector

        outputs = self.decoder_softmax_output(decoder_output_forward)  # Shape = (N, 1, input dims)

        if self.attention:
            self.inference_decoder = Model(
                [self.decoder_inputs, self.decoder_style_label, attention_encoder_states] + decoder_state_inputs,
                [outputs, attention_weights] + decoder_states)

        else:
            self.inference_decoder = Model([self.decoder_inputs, self.decoder_style_label] + decoder_state_inputs,
                                           [outputs] + decoder_states)

        print("Decoder inference model summary")
        print(self.inference_decoder.summary())

    def _run_inference(self, sequences, encoder_style_labels, decoder_style_labels,
                      id2word, sampler="greedy", sampler_params=0.5):
        """
        Runs the inference model on a single sequence.

        :param sequences: (1, Tx)
        :param encoder_style_labels: shape = (1, Num styles)
        :param decoder_style_labels: shape = (1, Num styles)
        :param id2word: dictionary to decode softmax
        :param sampler: how to decode softmax outputs - options include greedy, temperature or beam
        :param sampler_params: options for sampler. If temperature (0-1), if beam > 0
        :return: input_sequences (N, Tx), decoded sequences, attention matrix
        """

        if sampler != "greedy" and sampler != "temperature" and sampler != "beam":
            raise Exception("Valid options for the sampler are 'greedy', 'temperature' or 'beam'.")

        if sampler == "temperature" and (sampler_params < 0.0 or sampler_params > 1.0):
            raise Exception("When using temperature sampler, sampler_params should be between 0 and 1.")

        if sampler == "beam" and type(sampler_params) != int and sampler_params > 0:
            raise Exception("When using beam sampler, sampler_params should be an integer greater than 1.")

        encoder_states, state_h, state_c = self.inference_encoder.predict(
                                x=[sequences, encoder_style_labels])  # Expect this to be shape(N, Tx, lde)
        states = [state_h, state_c]

        if sampler == "beam":
            return self._beam_search_inference(encoder_states, states, k=sampler_params)

        stop_condition = False
        sentence = []
        attention = []
        t = 0

        # Create decoder input dims
        if self.decoder_embedding:
            target_seq = np.array([[self.word2id[self.start_character]]])
        else:
            target_seq = np.zeros((1, 1, self.input_dims))
            target_seq[0, 0, self.word2id[self.start_character]] = 1

        while not stop_condition:

            # # Populate the first character of target sequence with the start character
            if self.attention:
                softmax_output, attention_weights, state_h, state_c = self.inference_decoder.predict(
                    x=[target_seq, decoder_style_labels, encoder_states] + states)

            else:
                softmax_output, state_h, state_c = self.inference_decoder.predict(
                    x=[target_seq, decoder_style_labels] + states)

            if sampler == "greedy":
                sampled_token_index = greedy_sampling(softmax_output)

            elif sampler == "temperature":
                if sampler_params is not None:
                    sampled_token_index = temperature_sampling(softmax_output, sampler_params)
                else:
                    sampled_token_index = temperature_sampling(softmax_output)

            word = id2word[sampled_token_index]
            sentence.append(word)

            if self.attention:
                attention.append(attention_weights)

            t += 1
            if word == "<eos>" or t >= self.max_timestep:
                stop_condition = True
            states = [state_h, state_c]

            if self.decoder_embedding:
                target_seq = np.array([[sampled_token_index]])
            else:
                target_seq = np.zeros((1, 1, self.input_dims))
                target_seq[0, 0, sampled_token_index] = 1

        if self.attention:
            return sentence, np.asarray(attention).squeeze()
        else:
            return sentence

    def _beam_search_inference(self, all_encoder_outputs, encoder_final_states, k=3):
        # First place to start is the start_index
        states = encoder_final_states
        potential_candidates = [
            [[self.word2id[self.start_character]], 0,
             states, []]]  # this will store all the candidates and their probabilities and decoder states
        successful_candidates = []

        stop_index = self.word2id[self.stop_character]

        while k > 0:

            # Step 1: decode next token
            # Should be max K iterations of this for loop
            prob_options = []
            for seq_id, candidates in enumerate(potential_candidates):
                next_token = np.zeros((1, 1, self.input_dims))
                next_token[0, 0, candidates[0][-1]] = 1  # Get latest character in the sequence

                next_states = candidates[2]  # States generated so far
                current_prob = candidates[1]  # Current probability of sequence

                if self.attention:
                    output_tokens, attention_weights, h, c = self.inference_decoder.predict(
                        x=[next_token, all_encoder_outputs] + next_states)

                else:
                    output_tokens, h, c = self.inference_decoder.predict(x=[next_token] + next_states)

                states = [h, c]

                probs = np.log(output_tokens) + current_prob  # Length size of vocabulary
                probs = probs.squeeze()  # Convert to a list of (X, )

                # This stores as a tuple the possible probabilities, the next token and which sequence it belongs to
                # Token is the id for each word
                if self.attention:
                    prob_options += [(probability, token, seq_id, states, attention_weights) for token, probability in enumerate(probs)]
                else:
                    prob_options += [(probability, token, seq_id, states) for token, probability in enumerate(probs)]

            prob_options = sorted(prob_options, key=itemgetter(0), reverse=True)

            # Pick k top candidates
            new_candidates = [0] * k

            for i in range(k):
                seq_id = prob_options[i][2]
                token = prob_options[i][1]
                probability = prob_options[i][0]
                states = prob_options[i][3]
                attention = prob_options[i][4]
                new_seq = potential_candidates[seq_id][0] + [token]  # [([sequence], prob, states), ...]

                if self.attention:
                    all_attention = potential_candidates[seq_id][3] + [attention]
                    new_candidates[i] = [new_seq, probability, states, all_attention]
                else:
                    new_candidates[i] = [new_seq, probability, states]

                if len(new_seq) >= self.max_timestep or token == stop_index:
                    k -= 1
                    successful_candidates.append([new_seq, probability])

            potential_candidates = new_candidates

        prob_rankings = list(map(lambda x: x[1] / len(x[0]), successful_candidates))
        best_index = np.argmax(prob_rankings)
        best_prob = np.exp(prob_rankings[best_index])
        best_seq = successful_candidates[best_index][0]

        if self.attention:
            return best_seq, successful_candidates[best_index][3]
        else:
            return best_seq

    def inference(self, x_test, id2word, sampler="greedy", sampler_params=None):
        """
        This runs an inference on many data points where x_test is in the format
        x_test = [sequences, emotion, desired_emotion]
        """

        all_outputs = []
        all_attention = []
        for i in range(x_test[0].shape[0]):
            input_sent = x_test[0][i:i + 1]
            input_emotion = x_test[1][i:i + 1]
            desired_emotion = x_test[3][i:i + 1]

            # might need to reshape to (1, x)
            outputs, attention = self._run_inference(input_sent,
                                                    input_emotion,
                                                    desired_emotion,
                                                    id2word,
                                                    sampler=sampler,
                                                    sampler_params=sampler_params)

            all_outputs.append(outputs)
            all_attention.append(attention)

        return all_outputs, all_attention

    def visualise_attention(self, inputs, outputs, attention, normalise=True):
        """
        Visualises a heatmap of attention

        :param inputs: (Tx, ) string
        :param outputs: (Ty, ) string
        :param attention: (Tx, Ty)
        :return: figure to plot
        """

        fig, ax = plt.subplots(figsize=(10, 10))

        # Normalise data
        if normalise:
            for i in range(attention.shape[0]):
                attention[i] = attention[i] / np.max(attention[i])

        im = ax.imshow(attention,  cmap="gray")

        # Set colour bar
        cbar = ax.figure.colorbar(im, ax=ax, cmap="gray")

        ax.set_xticks(np.arange(attention.shape[1]))
        ax.set_yticks(np.arange(attention.shape[0]))

        ax.set_xticklabels(inputs)
        ax.set_yticklabels(outputs)
        ax.set_title("Attention Heatmap")
        ax.set_ylabel("Output")
        ax.set_xlabel("Input")
        ax.axis('tight')

        return fig, ax