"""
Different samplers for sentence decoding
"""

import numpy as np
from operator import itemgetter


def greedy_sampling(softmax_distribution):
    return np.argmax(softmax_distribution)


def temperature_sampling(softmax_distribution, temperature=0.5):

    preds = np.log(softmax_distribution) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    preds = preds.astype(np.float64) # deals with the multinomial error function saying sum is greater than 1. happens because of cast errors.
    preds = preds / np.sum(preds)
    probs = np.random.multinomial(1, preds.squeeze(), 1)  # Squeeze to get a 1d array
    return np.argmax(probs)