"""
Cleans the tweets using the TweetPreProcessor class.

Author: John Lingi
Date: 06/18
"""
from preprocessing.tweet_preprocessor import TweetPreProcessor
from ekphrasis.classes.tokenizer import Tokenizer
from ekphrasis.dicts.emoticons import emoticons
from preprocessing.config import emoji_unicode
from utils.file_io import load_raw_data, load_from_text
import os

if __name__ == "__main__":

    text_processor = TweetPreProcessor(
        # terms that will be omitted - omit after annotation
        omit=['url', 'email', 'percent', 'number', 'hashtag', 'allcaps', 'elongated', 'repeated', 'emphasis'],

        # terms that will be normalized
        normalize=['email', 'percent', 'money', 'phone', 'user',
                   'time', 'url', 'date', 'number'],

        # terms that will be annotated
        annotate={"hashtag", "elongated", "repeated",
                  "emphasis", "censored"},
        fix_html=True,  # fix HTML tokens

        # corpus from which the word statistics are going to be used
        # for word segmentation
        segmenter="twitter",

        # corpus from which the word statistics are going to be used
        # for spell correction
        corrector="twitter",

        unpack_hashtags=True,  # perform word segmentation on hashtags
        unpack_contractions=True,  # Unpack contractions (can't -> can not)
        spell_correct_elong=True,  # spell correction for elongated words

        # select a tokenizer. You can use SocialTokenizer, or pass your own
        # the tokenizer, should take as input a string and return a list of tokens
        tokenizer=Tokenizer(lowercase=True).tokenize,

        # list of dictionaries, for replacing tokens extracted from the text,
        # with other expressions. You can pass more than one dictionaries.
        dicts=[emoticons, emoji_unicode]
    )

    testing = False
    if testing:
        tweets = [
            "her's is better.than st. john's it's 60th, 1st, 2nd, 100 species",
            "Me and these @burns 20% that f*ck I pick up.. off the pitches don't get on at all like 😢😢 #sting",
            "CANT WAIT for the new season of #TwinPeaks ＼(^o^)／!!! #davidlynch #tvseries :)))",
            "I saw the new #johndoe movie and it suuuuucks!!! she's WAISTED $10... #badmovies :/",
            "@SentimentSymp:  can't wait for the Nov 9 #Sentiment talks!  YAAAAAAY !!! :-D :-D http://sentimentsymposium.com/."
        ]

        for tweet in tweets:
            print(text_processor.pre_process_doc(tweet))

    else:

        current_dir = os.getcwd()
        source_dir = os.path.dirname(current_dir)

        emotions = ["joy"]
        # datasets = ["train", "test", "val"]
        root = source_dir + "/data/scraped/"

        # words = [
        #     "cheerful",
        #     "delight",
        #     "beautiful",
        #     "best",
        #     "exhilarating",
        #     "excited",
        #     "happy",
        #     "jubilant",
        #     "laughter",
        #     "playful",
        #     "rejoicing",
        #     "smiling",
        #     "amazing",
        #     "blessed",
        #     "enjoyable",
        #     "joy",
        #     "laugh",
        #     "laughing"
        # ]

        words = [str(i) for i in range(0,7)]

        for emotion in emotions:
            # for dataset in datasets:

            # raw_file = root + "raw/" + emotion + "-" + dataset + ".txt"
            # tweets, labels = load_raw_data(raw_file)

            # raw_file = root + "raw/" + emotion + "elated-processed-10.txt"

            for word in words:
                raw_file = root + "raw/emoji-raw-tweets-" + word + ".txt"
                tweets = load_from_text(raw_file)
                # save_file = root + "processed/" + emotion + "-processed.txt"
                save_file = root + "processed/emoji-processed-tweets" + word + ".txt"
                with open(save_file, "w") as f:
                    print("Writing data to", save_file)

                    for index, tweet in enumerate(tweets):

                        tokenised_tweet = text_processor.pre_process_doc(tweet)
                        tokenised_tweet = " ".join(tokenised_tweet)
                        f.write(tokenised_tweet + '\n')

                f.close()
