"""
Function to remove duplicate tweets in a file
"""

import pandas as pd
from Levenshtein import seqratio
import os


class CleanTweets(object):
    def __init__(self, file_list=None, load_from_df=False, df=None, delimeter='\t', header=None, names=["tweet"]):
        """

        :param file_list:
        :param delimeter:
        :param header:
        :param names: column names to use if header is None. This should be None if a header exists
        """

        if load_from_df and df is not None:
            self.df = df
            self.df.columns=["tweet"]
            print("Number of tweets loaded:", self.df.shape[0])

        elif load_from_df and df is None:
            print("'load_from_df' is set True. Expected a dataframe object to load from.")
            raise Exception

        elif file_list is not None:
            self.df = pd.DataFrame()
            for file in file_list:

                temp_df = pd.read_csv(file, delimiter=delimeter, header=header, names=names)
                print("Loaded {0} tweets from {1}".format(temp_df.shape[0], file))

                self.df = pd.concat((self.df, temp_df), axis=0, ignore_index=True)

            self.df = self.df.dropna(subset=["tweet"], axis=0)
            self.df.reset_index(inplace=True, drop=True)

            print("Number of tweets loaded:", self.df.shape[0])

        else:
            print("Must specify if either loading from file or dataframe")
            raise Exception

    @staticmethod
    def levenshtein_similarity(s1, s2):
        """
        Returns percentage similarity between sequences s1 and s2
        """
        try:
            return seqratio(s1, s2)
        except:
            return -1

    def remove_duplicates(self, similarity_distance_threshold=0.8):
        old = self.df.shape[0]
        indexes = []
        print(self.df.index)
        for row in range(1, old):
            # Assume similar rows are next to each other
            if self.levenshtein_similarity(self.df.loc[row]["tweet"], self.df.loc[row-1]["tweet"]) >= similarity_distance_threshold:
                indexes.append(row)

        self.df = self.df.drop(labels=indexes, axis=0)

        new = self.df.shape[0]
        print("Removed", old-new, "exact duplicates.")

    def save_to_csv(self, file_path, index=None, sep='\t'):
        return self.df.to_csv(file_path, index=index, sep=sep, header=False)

    def remove_by_length(self, limit=10):
        old = self.df.shape[0]
        remove_func = lambda x: (len(x.split()) <= limit)

        print(self.df["tweet"])
        indexes = self.df["tweet"].apply(remove_func)

        self.df = self.df[indexes]
        self.df.reset_index(inplace=True, drop=True)
        new = self.df.shape[0]
        print("Removed", old - new, "sentences with length less than or equal to", limit)


if __name__ == "__main__":
    current_dir = os.getcwd()
    source_dir = os.path.dirname(current_dir)

    # files = [
    #     source_dir + "/data/scraped/anger-processed-10-tokens.txt",
    #     source_dir + "/data/semeval18/processed/anger-train-processed.txt",
    #     source_dir + "/data/semeval18/processed/anger-val-processed.txt",
    #     source_dir + "/data/semeval18/processed/anger-test-processed.txt"
    # ]

    # words = [
    #     "cheerful",
    #     "delight",
    #     "beautiful",
    #     "best",
    #     "exhilarating",
    #     "excited",
    #     "happy",
    #     "jubilant",
    #     "laughter",
    #     "playful",
    #     "rejoicing",
    #     "smiling",
    #     "amazing",
    #     "blessed",
    #     "enjoyable",
    #     "joy",
    #     "laugh",
    #     "laughing"
    # ]

    words = [str(i) for i in range(0, 7)]

    for word in words:
        files = [source_dir + "/data/scraped/processed/emoji-processed-tweets-" + word + ".txt"]

        ct = CleanTweets(files)
        ct.remove_by_length()
        ct.remove_duplicates()
        ct.save_to_csv(source_dir + "/data/scraped/processed/emoji-processed-tweets-" + word + "-10-tokens.txt")
        # df = ct.get_df()
        # df.to_csv("../data/scraped/sadness_tweets_duplicates_removed.txt", index=None, sep='\t')
