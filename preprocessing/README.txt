Scripts used to clean raw tweets. Currently this is not being used as the data has already been pre-processed.

In addition, the Ekphrasis package has not been saved in this workspace. We will have to create a download link for
the future.