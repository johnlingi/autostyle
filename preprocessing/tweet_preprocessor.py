"""
This file extends the preprocessor class in the Ekphrasis code.

In addition, we have extended the unpack_contractions function to handle, "'s" terms and "@ to 'at'"

Author: John Lingi
Date: 06/18
"""
import re
import ftfy
from ekphrasis.classes.preprocessor import TextPreProcessor
from ekphrasis.utils.nlp import unpack_contractions
from preprocessing.config import curse_words
import string


class TweetPreProcessor(TextPreProcessor):

    @staticmethod
    def remove_repeating_tags(wordlist):
        """
        Remove repeating tags such as emojis or normalised words.

        Do not affect emotion by repeating
        :param wordlist:
        :return:
        """

        text = []
        for index, token in enumerate(wordlist):
            if index > 0 and token == text[-1]:
                continue
            else:
                text.append(token)

        return text

    @staticmethod
    def curse_word_replace(wordlist):
        """
        Replace all curse words as defined in curse_words list
        :param wordlist:
        :return:
        """
        sub_token = "<curse>"
        text = []

        for index, token in enumerate(wordlist):
            if token in curse_words:
                text.append(sub_token)

            elif token == "<censored>":
                text[-1] = sub_token

            else:
                text.append(token)

        return text

    @staticmethod
    def punct_replace(wordlist):
        """
        Replace all punctuation except
        :param wordlist:
        :return:
        """
        to_keep = ".!?"
        text = []

        for index, token in enumerate(wordlist):
            if token == "s" and index > 0 and wordlist[index-1] == "'":
                text.append("'s")

            elif token in string.punctuation and token not in to_keep:
                continue

            else:
                text.append(token)

        return text

    @staticmethod
    def unknown_replace(wordlist):
        """
        Replaces unknown characters that are not in the emoji dictionaries or alphanumeric or punctuation

        :param wordlist:
        :return:
        """

        text = []
        special = "'s"

        for index, token in enumerate(wordlist):
            if token.isalnum():
                text.append(token)

            elif token in string.punctuation:
                text.append(token)

            elif token == special:
                text.append(token)

            elif token[0] == "<" and token[-1] == ">": # tags are marked <****>
                text.append(token)

            else:
                continue

        return text

    def pre_process_doc(self, doc):
        """
        Changes to Ekphrasis code are:
            1. Set unpack_hashtags to true regardless of if in 'omit' parameter or not
            2. Moved omit process to the last stage in order to remove annotated tags

        :param doc:
        :return:
        """

        self.unpack_hashtags = True
        doc = re.sub(r' +', ' ', doc)  # remove repeating spaces

        # ###########################
        # # fix bad unicode
        # ###########################
        # if self.fix_bad_unicode:
        #     doc = textacy.preprocess.fix_bad_unicode(doc)
        #
        # ###########################
        # # fix html leftovers
        # ###########################
        # doc = html.unescape(doc)

        ###########################
        # Fix text
        ###########################
        if self.fix_text:
            doc = ftfy.fix_text(doc)

        ###########################
        # Backoff
        ###########################
        for item in self.backoff:
            # better add an extra space after the match.
            # Just to be safe. extra spaces will be normalized later anyway
            doc = self.regexes[item].sub(lambda m: " " + "<" + item + ">" + " ",
                                         doc)
        #        for item in self.omit:
        #            doc = doc.replace("<" + item + ">", '')

        ###########################
        # Unpack hashtags
        ###########################
        if self.unpack_hashtags:
            doc = self.regexes["hashtag"].sub(
                lambda w: self.handle_hashtag_match(w), doc)

        ###########################
        # Handle special cases
        ###########################
        if self.mode != "fast":
            if "allcaps" in self.include_tags:
                doc = self.regexes["allcaps"].sub(
                    lambda w: self.handle_generic_match(w, "allcaps",
                                                        mode=self.all_caps_tag),
                    doc)

            if "elongated" in self.include_tags:
                doc = self.regexes["elongated"].sub(
                    lambda w: self.handle_elongated_match(w), doc)

            if "repeated" in self.include_tags:
                doc = self.regexes["repeat_puncts"].sub(
                    lambda w: self.handle_repeated_puncts(w), doc)

            if "emphasis" in self.include_tags:
                doc = self.regexes["emphasis"].sub(
                    lambda w: self.handle_emphasis_match(w), doc)

            if "censored" in self.include_tags:
                doc = self.regexes["censored"].sub(
                    lambda w: self.handle_generic_match(w, "censored"), doc)

        ###########################
        # Unpack contractions: i'm -> i am, can't -> can not...
        ###########################

        # remove textacy dependency
        if self.unpack_contractions:
            doc = unpack_contractions(doc)

        # omit allcaps if inside hashtags
        doc = re.sub(r' +', ' ', doc)  # remove repeating spaces
        # doc = re.sub(r'<hashtag><allcaps>', '<hashtag>', doc)  # remove repeating spaces
        # doc = doc.replace('<hashtag> <allcaps>', '<hashtag>')
        # doc = doc.replace('</allcaps> </hashtag>', '</hashtag>')

        ###########################
        # Omit
        ###########################

        for item in self.omit:
            doc = doc.replace("<" + item + ">", '')
            doc = doc.replace("</" + item + ">", '')

        ###########################
        # Tokenize
        ###########################
        doc = self.remove_hashtag_allcaps(doc.split())
        doc = " ".join(doc)  # normalize whitespace
        if self.tokenizer:
            doc = self.tokenizer(doc)
            # Replace tokens with special dictionaries (slang,emoticons ...)
            # todo: add spell check before!
            if self.dicts:
                for d in self.dicts:
                    doc = self.dict_replace(doc, d)

            # Replace curse words
            doc = self.curse_word_replace(doc)

            # Remove repeating tags
            doc = self.remove_repeating_tags(doc)

            # Remove punctuation that is not ".,?!" and "'s"
            doc = self.punct_replace(doc)

            # Remove unknown emojis
            doc = self.unknown_replace(doc)

        return doc


